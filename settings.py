""" ## ID CANALE 'ACCETTO' REGOLAMENTO##

L'ID del canale dove gli utenti devono
accettare il regolamento.
 (default: ) """
accetto_channel_id = 

""" ## ID RUOLO NON VERIFICATO ##

Viene assegnato automaticamente quando un utente
accetta il regolamento ma non è stato ancora ve-
-rificato. (default: ) """
not_verified_id = 

""" ## ID CANALE LOG REGOLAMENTO ##

Canale in cui il bot invierà il log del regolamento
(es. un utente che accetta le regole.) 
(default: ) """
rules_log_id = 

""" ## ID CANALE PER /JOIN BOT REGISTRAZIONE ##

Canale in cui il bot invierà il /join per
far entrare il bot nel canale per iniziare la
registrazione della partita 
(default: ) """
record_bot_join_channel = 

""" ## ID CANALE VOCALE QUIZ VIOLA ##

Canale vocale predefinito ove giocano i viola
 (default: ) """
violet_main_channel = 

""" ## ID CANALE VOCALE QUIZ VERDI ##

Canale vocale predefinito ove giocano i verdi
 (default: ) """
green_main_channel = 
