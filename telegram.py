class TelegramEvent:

    def __init__(self, loop):
        self.handlers = {}
        self.session = ''
        self.api_id = 
        self.api_hash = ''
        self.loop = loop

    def run(self):
        from pyrogram import Client, Filters

        # TODO: Creare account Telegram specifico
        app = Client(self.session,
                     api_id=self.api_id,
                     api_hash=self.api_hash)

        @app.on_message(Filters.chat("LiveQuizNotifiche") & Filters.regex("LIVE NOW"))
        async def quiz_handler(client, message):
            await self.call("new_quiz")

        self.loop.create_task(self.main(app))

    @staticmethod
    async def main(app):
        await app.start()

    async def call(self, tipo):
        if tipo in self.handlers:
            for h in self.handlers[tipo]:
                await h()

    def event(self, tipo):
        def registerhandler(handler):
            if tipo in self.handlers:
                self.handlers[tipo].append(handler)
            else:
                self.handlers[tipo] = [handler]
            return handler
        return registerhandler
