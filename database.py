from pymongo import MongoClient  # pymongo --> pip/pip3 install pymongo
import discord
import datetime
from stats import PlotUpload
import asyncio
from random import randint


class MongoDB(MongoClient):

    def __init__(self, **kwargs):
        super().__init__()  # 
        self.database = self[kwargs["database"]]
        self.image_plot = PlotUpload()

    def upload(self, collection_name, post, query, replace=True, no_count=False):
        coll = self.database[collection_name]

        has_already_document = coll.count_documents(query)

        if has_already_document and not no_count:
            if replace:
                coll.replace_one(query, post)
        else:
            coll.insert_one(post)

        return

    def download(self, collection_name, query, delete=True):
        coll = self.database[collection_name]
        has_document = coll.count_documents(query)
        if has_document:
            document = coll.find_one(query)
            if delete:
                coll.delete_one(query)
            return document
        else:
            return None

    def random_doc(self, collection_name):
        coll = self.database[collection_name]
        doc_list = []
        for doc in coll.find():
            doc_list.append(doc)

        try:
            choose = doc_list[randint(0, len(doc_list) - 1)]
        except ValueError:
            return
        else:
            return choose

    def drop(self, collection_name):
        coll = self.database[collection_name]
        coll.drop()
        return

    async def update_text_stats(self, guild: discord.Guild):
        image_plot = self.image_plot
        collection_name = str(guild.id) + ".text_stats"
        self.drop(collection_name)

        #  mesi passati dalla creazione della guild
        elapsed_month = ((datetime.datetime.now().year - guild.created_at.year) * 12 +
                         (datetime.datetime.now().month - guild.created_at.month)) + 1

        x_axis_list = [0] * elapsed_month
        kword = ["mi muto", "gioco muto", "devo mutarmi", "sono mutato", "mi silenzio"]
        message_list = []
        for channel in guild.text_channels:
            message_list = message_list + await channel.history(limit=None, oldest_first=True).flatten()

        for member in guild.members:
            if member.top_role == guild.default_role:
                continue

            joined_index = ((member.joined_at.year - guild.created_at.year) * 12 +
                            (member.joined_at.month - guild.created_at.month))

            text_stats = {
                "member_id": member.id,
                "counter": 0,
                "muted_counter": 0,
                "embed_preview": "",
                "embed_detailed": "",
                "plot_data": {
                    "j_index": joined_index,
                    "x": x_axis_list,
                    "y": [0] * elapsed_month,
                    "m": [0] * elapsed_month
                }
            }

            for message in message_list:

                dyn_elapsed_month = ((message.created_at.year - guild.created_at.year) * 12 +
                                     (message.created_at.month - guild.created_at.month))

                if not x_axis_list[dyn_elapsed_month]:
                    x_axis_list[dyn_elapsed_month] = message.created_at.strftime("%b %y")

                if message.author == member:
                    text_stats["counter"] += 1
                    for word in kword:
                        if message.content.lower().find(word) != -1:
                            text_stats["plot_data"]["m"][dyn_elapsed_month] += 1
                            text_stats["muted_counter"] += 1

                    text_stats["plot_data"]["y"][dyn_elapsed_month] += 1

            user = member.name + "#" + member.discriminator
            text_stats["embed_preview"] = image_plot.tstats_embed_previw(text_stats['plot_data'],
                                                                         member_id=member.id)
            await asyncio.sleep(0.5)
            text_stats["embed_detailed"] = image_plot.tstats_embed_detailed(text_stats['plot_data'],
                                                                            user, member_id=member.id)
            query = {"member_id": member.id}
            post = text_stats
            self.upload(collection_name, post, query)
