import asyncio
import discord  # discord.py --> pip/pip3 install -U discord.py
from discord.ext import commands  # discord.py --> pip/pip3 install -U discord.py
from database import MongoDB
from stats import PlotUpload, RandomUserWebHook, stringify, wrong_text_log
from telegram import TelegramEvent
import sys
import traceback
from async_timeout import timeout
from functools import partial
import youtube_dl
import itertools
import settings
import datetime
import pytz

# Suppress noise about console usage from errors
youtube_dl.utils.bug_reports_message = lambda: ''


ytdl_format_options = {
    'format': 'bestaudio/best',
    'outtmpl': 'tmp/%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0'  # bind to ipv4 since ipv6 addresses cause issues sometimes
}

ffmpeg_options = {
    'before_options': "-nostdin -reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5",
    'options': '-vn'
}

ytdl = youtube_dl.YoutubeDL(ytdl_format_options)


bot = commands.Bot(command_prefix='>')
telegram_client = TelegramEvent(bot.loop)


class VoiceConnectionError(commands.CommandError):
    """Custom Exception class for connection errors."""


class InvalidVoiceChannel(VoiceConnectionError):
    """Exception for cases of invalid Voice Channels."""


@bot.event
async def on_ready():
    """Eseguita quando il bot è pronto"""
    await bot.change_presence(activity=discord.Activity(name='#Guild Stats', type=discord.ActivityType.watching))
    bot.add_cog(Music(bot))
    print('Logged in as: {0} [{1}]'.format(bot.user.name, bot.user.id))
    print('------')


@bot.event
async def on_message(message):
    """Cosa fare in caso di nuovo messaggio"""
    # Controlla se l'autore non sono io e se non è un PM
    if message.author == bot.user or message.guild is None:
        return

    # Controlla se si tratta di un admin
    try:
        is_admin = message.author.top_role.permissions.administrator
    except AttributeError:
        is_admin = False

    # Controlla se il messaggio è sul canale di 'accetto'
    # e se non è un admin
    if message.channel.id == settings.accetto_channel_id and not is_admin:
        if message.content.lower() == "accetto":
            bot.loop.create_task(on_accepted_rules(message))
        else:
            try:
                emoji = '\N{THUMBS DOWN SIGN}'
                await message.add_reaction(emoji)
            except discord.errors.NotFound:
                print("Impossibile aggiungere reaction: messaggio cancellato dall'utente!")
            finally:
                wrong_text_log(bot, message)

        bot.loop.create_task(message.delete(delay=1))  # Cancella il messaggio dopo delay[sec]

    await bot.process_commands(message)  # Elabora i comandi, se ve ne sono


async def on_accepted_rules(message):
    """Eseguita quando qualcuno accetto il regolamento"""
    member = message.author
    guild = message.guild
    coll_name = str(guild.id) + ".roles"  # usa collezione "guild.id"
    query = {"user_id": message.author.id}

    document = db_client.download(coll_name, query)
    if document is not None:
        role_list = document["roles"]
        old_roles_id = [role_list[role]["id"] for role in range(len(role_list))]
        old_roles = [guild.get_role(i) for i in old_roles_id if guild.get_role(i)]
    else:
        old_roles = [guild.get_role(settings.not_verified_id)]

    # TODO: Velocizzare il prcesso di aggiornamento ruoli
    try:
        # ATTENZIONE: Imposta correttamente la gerarchia dei ruoli per evitare modifiche ai ruoli degli admin
        await member.edit(roles=old_roles, reason="Ha accettato il regolamento.")
    except discord.errors.Forbidden:
        print(f"Impossibile editare i ruoli di '{member}': Permesso negato!")
    else:
        # RECUPERA LA VERSIONE DEL REGOLAMENTO NEL DATABASE
        version = db_client.download(coll_name, {"type": "version"}, delete=False)["version"]
        log_channel = guild.get_channel(settings.rules_log_id)
        roles_string = stringify([role.name for role in old_roles])

        embed = discord.Embed(title="**Ha appena accettato il regolamento** ✅", colour=discord.Colour(0xad04c1),
                              description=f"Ha scritto:```{message.content}```",
                              timestamp=datetime.datetime.now(tz))

        embed.set_thumbnail(url=member.avatar_url)
        embed.set_author(name=str(member), icon_url=member.avatar_url)
        embed.set_footer(text=f"ID: {member.id}", icon_url=guild.icon_url)
        embed.add_field(name="Quando:", value=f"```{message.created_at.astimezone(tz).strftime('%d-%m-%y %H:%M')}```")
        embed.add_field(name="Nel canale:", value=f"```{message.channel.name}```")
        embed.add_field(name="Versione:\n", value=f"```{float(version)}```")
        embed.add_field(name="Ruoli restituiti:\n", value=f"```{roles_string}```")
        bot.loop.create_task(log_channel.send(embed=embed))


@bot.command(hidden=True, description="Aggiorna il database delle text_stats")
@commands.guild_only()
@commands.has_permissions(administrator=True)
async def update_db(ctx, which_collection: str):
    if which_collection == "text_stats":
        await ctx.send("Un attimo...")
        start = datetime.datetime.now()
        await db_client.update_text_stats(ctx.guild)
        await ctx.send("Fatto! Ci ho messo **{}**s.".format((datetime.datetime.now() - start).seconds))


# COMMAND GROUP: alert_dict
# Commands list:
# - new: Utile per aggiornare il regolamento ad una nuova versione

@bot.group(hidden=True, description="Gestione delle regole")
@commands.guild_only()
@commands.has_permissions(administrator=True)
async def rules(ctx):
    """Gestione delle regole"""
    if ctx.invoked_subcommand is None:
        await ctx.send(f'**{ctx.subcommand_passed}** è un sotto-comando non valido...')


@rules.command(hidden=True, description='Resetta i ruoli di tutti gli utenti in base alla gerarchia')
async def new(ctx, version: float):
    """Salva i ruoli nel DB e li resetta"""
    command = ctx.message
    await command.channel.send("Un attimo...")
    coll_name = str(command.guild.id) + ".roles"  # usa collezione "guild.id"

    # AGGIORNA LA VERSIONE DEL REGOLAMENTO NEL DATABASE
    db_client.upload(coll_name, {"type": "version", "version": float(version)}, {"type": "version"}, replace=True)

    for member in command.guild.members:
        if not member.bot:
            roles = member.roles  # user roles
            post = {"username": member.name,  # username
                    "user_id": member.id,  # user id
                    "roles": [{"name": roles[role].name, "id": roles[role].id} for role in range(len(member.roles))]
                    }
            # Save roles on MongoDB's collection: discod/roles/
            query = {"user_id": post["user_id"]}
            has_role = bool(len(member.roles) - 1)
            if has_role:
                db_client.upload(coll_name, post, query)

            # TODO: Velocizzare il prcesso di aggiornamento ruoli
            try:
                # ATTENZIONE: Imposta correttamente la gerarchia dei ruoli per evitare modifiche ai ruoli degli admin
                await member.edit(roles=[], reason="Deve accettare il nuovo regolamento.")
            except discord.errors.Forbidden:
                print(f"Impossibile editare i ruoli di '{member}': Permesso negato!")

    await command.channel.send("{} Ho aggiornato i ruoli della guild!".format(command.author.mention))


# COMMAND GROUP: alert_dict
# Commands list:
# - new: Utile per aggiungere frasi di allerta di un nuovo quiz

@bot.group(hidden=True, description="Utile per aggiungere frasi di allerta di un nuovo quiz")
@commands.guild_only()
@commands.has_permissions(administrator=True)
async def alert_dict(ctx):
    """Utile per aggiungere frasi di allerta di un nuovo quiz"""
    if ctx.invoked_subcommand is None:
        await ctx.send(f'**{ctx.subcommand_passed}** è un sotto-comando non valido...')


@alert_dict.command(hidden=True, description="Utile per aggiungere frasi di allerta di un nuovo quiz")
async def new(ctx, member: discord.Member, *, phrase: str):
    """Utile per aggiungere frasi di allerta di un nuovo quiz"""

    phrase = phrase.replace("!#", "@everyone")
    await ctx.send(f"```{member.name + ': ' + phrase}```\nVa bene? Scrivi **Si** per accettare,"
                   f" altrimenti non fare nulla.")

    try:
        await bot.wait_for('message',
                           check=lambda m: m.author == ctx.author and
                           m.channel == ctx.channel and
                           m.content.lower() == "si",
                           timeout=20)
    except asyncio.TimeoutError:
        await ctx.send("Niente allora...")
        return
    else:
        coll_name = str(ctx.guild.id) + ".alert_dict"

        post = {"user_id": member.id,
                "username": member.name,
                "phrase": phrase
                }

        # AGGIUNGE LA FRASE NEL DATABASE
        db_client.upload(coll_name, post, {}, replace=False, no_count=True)
        await ctx.send("Aggiunta :laughing:")


# COMMAND GROUP: user
# Commands list:
# - joined: Verifica quando è entrato un utente
# - text_stats: Recupera statistiche dalla chat per l'utente

@bot.group(hidden=True, name='user', description="Alcune informazioni sull'utente")
@commands.guild_only()
@commands.has_permissions(administrator=True)
async def _user(ctx):
    """Alcune informazioni sull'utente"""
    if ctx.invoked_subcommand is None:
        await ctx.send(f'**{ctx.subcommand_passed}** è un sotto-comando non valido...')


@_user.command(description='Verifica quando è entrato un utente')
async def joined(ctx, member: discord.Member):
    """Verifica quando è entrato un utente"""
    joined_at = member.joined_at.strftime("%d/%m/%Y (%a) alle %H:%M:%S")
    embed = discord.Embed(title=f"***{member}***  è con noi dal:", colour=discord.Colour(0x5eb800),
                          description=f"**```{joined_at}```**", timestamp=datetime.datetime.now(tz))
    embed.set_thumbnail(url=member.avatar_url)
    embed.set_author(name=f"{member}", icon_url=member.avatar_url)
    embed.set_footer(text=f"ID: {member.id}", icon_url=ctx.guild.icon_url)

    await ctx.send(embed=embed)


@_user.command(description="Recupera statistiche dalla chat per l'utente")
async def text_stats(ctx, member: discord.Member):
    """Alcune statistiche per l'utente (testo)"""
    async with ctx.channel.typing():

        collection_name = str(ctx.guild.id) + ".text_stats"
        query = {"member_id": member.id}
        payload = db_client.download(collection_name, query, delete=False)

        if payload is not None:
            image_link = image_plot.upload_image(payload["embed_preview"])
            det_link = image_plot.upload_image(payload["embed_detailed"])
            joined_at = member.joined_at.strftime('%d/%m/%y')
            month_count = payload['plot_data']['y']
            last_month_count = month_count[len(month_count) - 1]

            embed = discord.Embed(title=f"**Numero totale di messaggi**", colour=discord.Colour(0xB900E1),
                                  description=f"Numero totale di messaggi nella guild **```\n{payload['counter']}```**",
                                  timestamp=datetime.datetime.now(tz), url=det_link)
            embed.set_image(url=image_link)
            embed.set_thumbnail(url=member.avatar_url)
            embed.set_author(name=str(member), icon_url=member.avatar_url)
            embed.set_footer(text=f"ID: {member.id}", icon_url=ctx.guild.icon_url)
            embed.add_field(name="Messaggi ultimo mese:", value=f"```{last_month_count}```", inline=True)
            embed.add_field(name="Ha detto di ***mutarsi***:", value=f"```{payload['muted_counter']}```", inline=True)
            embed.add_field(name="Presente nella guild da:", value=f"```{joined_at}```", inline=True)

            bot.loop.create_task(ctx.send(embed=embed))
        else:
            await ctx.send(f"**{member}** non c'è nel database, probabilmente non è un utente verificato.")


# COMMAND COG: music
# Commands list:
# - connect: il bot si collega ad un canale vocale
# - disconnect: il bot si disconnette del canale vocale
# - play: Riproduce della musica

class YTDLSource(discord.PCMVolumeTransformer):

    def __init__(self, source, *, data, requester):
        super().__init__(source)
        self.requester = requester

        self.title = data.get('title')
        self.web_url = data.get('webpage_url')

    def __getitem__(self, item: str):
        """Allows us to access attributes similar to a dict.
        This is only useful when you are NOT downloading.
        """
        return self.__getattribute__(item)

    @classmethod
    async def create_source(cls, ctx, url, *, loop=None, stream=False):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))

        if 'entries' in data:
            # take first item from a playlist
            data = data['entries'][0]

        await ctx.send(f'**Aggiunto alla coda** >> **`{data["title"]}`**.', delete_after=20)

        if not stream:
            source = ytdl.prepare_filename(data)
        else:
            return {'webpage_url': data['webpage_url'], 'requester': ctx.author, 'title': data['title']}

        return cls(discord.FFmpegPCMAudio(source, **ffmpeg_options), data=data, requester=ctx.author)

    @classmethod
    async def regather_stream(cls, data, *, loop=None):
        """Used for preparing a stream, instead of downloading.
        Since Youtube Streaming links expire."""
        loop = loop or asyncio.get_event_loop()
        requester = data['requester']

        to_run = partial(ytdl.extract_info, url=data['webpage_url'], download=False)
        data = await loop.run_in_executor(None, to_run)

        return cls(discord.FFmpegPCMAudio(data['url'], **ffmpeg_options), data=data, requester=requester)


class MusicPlayer:
    """A class which is assigned to each guild using the bot for Music.

    This class implements a queue and loop, which allows for different guilds to listen to different playlists
    simultaneously.

    When the bot disconnects from the Voice it's instance will be destroyed.
    """

    __slots__ = ('bot', '_guild', '_channel', '_cog', 'queue', 'next', 'current', 'np', 'volume')

    def __init__(self, ctx):
        self.bot = ctx.bot
        self._guild = ctx.guild
        self._channel = ctx.channel
        self._cog = ctx.cog

        self.queue = asyncio.Queue()
        self.next = asyncio.Event()

        self.np = None  # Now playing message
        self.volume = .5
        self.current = None

        ctx.bot.loop.create_task(self.player_loop())

    async def player_loop(self):
        """Our main player loop."""
        await self.bot.wait_until_ready()

        while not self.bot.is_closed():
            self.next.clear()

            try:
                # Wait for the next song. If we timeout cancel the player and disconnect...
                async with timeout(300):  # 5 minutes...
                    source = await self.queue.get()
            except asyncio.TimeoutError:
                return self.destroy(self._guild)

            if not isinstance(source, YTDLSource):
                # Source was probably a stream (not downloaded)
                # So we should regather to prevent stream expiration
                try:
                    source = await YTDLSource.regather_stream(source, loop=self.bot.loop)
                except Exception as e:
                    await self._channel.send(f"C'è stato un errore.\n"
                                             f'```css\n[{e}]\n```')
                    continue

            source.volume = self.volume
            self.current = source

            self._guild.voice_client.play(source, after=lambda _: self.bot.loop.call_soon_threadsafe(self.next.set))
            self.np = await self._channel.send(f'**`{source.requester}`** >> '
                                               f'**Sto riproducendo \U0001F3B6**: `{source.title}`',
                                               delete_after=20)
            await self.next.wait()

            # Make sure the FFmpeg process is cleaned up.
            source.cleanup()
            self.current = None

            try:
                # We are no longer playing this song...
                await self.np.delete()
            except discord.HTTPException:
                pass

    def destroy(self, guild):
        """Disconnect and cleanup the player."""
        return self.bot.loop.create_task(self._cog.cleanup(guild))


class Music(commands.Cog):
    """Music related commands."""

    __slots__ = ('bot', 'players')

    def __init__(self, bot_):
        self.bot = bot_
        self.players = {}

    async def cleanup(self, guild):
        try:
            await guild.voice_client.disconnect()
        except AttributeError:
            pass

        try:
            del self.players[guild.id]
        except KeyError:
            pass

    @staticmethod
    async def __local_check(ctx):
        """A local check which applies to all commands in this cog."""
        if not ctx.guild:
            raise commands.NoPrivateMessage
        return True

    @staticmethod
    async def __error(ctx, error):
        """A local error handler for all errors arising from commands in this cog."""
        if isinstance(error, commands.NoPrivateMessage):
            try:
                return await ctx.send('Questo comando non può essere usato in chat privata.')
            except discord.HTTPException:
                pass
        elif isinstance(error, InvalidVoiceChannel):
            await ctx.send('Impossibile connettersi al Canale Vocale.')

        print('Ignoring exception in command {}:'.format(ctx.command), file=sys.stderr)
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    def get_player(self, ctx):
        """Retrieve the guild player, or generate one."""
        try:
            player = self.players[ctx.guild.id]
        except KeyError:
            player = MusicPlayer(ctx)
            self.players[ctx.guild.id] = player

        return player

    @commands.command(name='connect', aliases=['join'])
    async def connect_(self, ctx, *, channel: discord.VoiceChannel = None):
        """Connect to voice.

        Parameters
        ------------
        ctx: Discord's context
        channel: discord.VoiceChannel [Optional]
            The channel to connect to. If a channel is not specified, an attempt to join the voice channel you are in
            will be made.

        This command also handles moving the bot to different channels.
        """
        if not channel:
            try:
                channel = ctx.author.voice.channel
            except AttributeError:
                raise InvalidVoiceChannel('Nessun canale a cui unirsi.')

        vc = ctx.voice_client

        if vc:
            if vc.channel.id == channel.id:
                return
            try:
                await vc.move_to(channel)
            except asyncio.TimeoutError:
                raise VoiceConnectionError(f'**Tempo scaduto per muoversi a**: `{channel}`')
        else:
            try:
                await channel.connect()
            except asyncio.TimeoutError:
                raise VoiceConnectionError(f'**Tempo scaduto per muoversi a**: `{channel}`')

        await ctx.send(f'**Connesso a** >> `{channel}`', delete_after=20)

    @commands.command(name='play', aliases=['sing'])
    async def play_(self, ctx, *, search: str):
        """Request a song and add it to the queue.

        This command attempts to join a valid voice channel if the bot is not already in one.
        Uses YTDL to automatically search and retrieve a song.

        Parameters
        ------------
        ctx: Discord's context
        search: str [Required]
            The song to search and retrieve using YTDL. This could be a simple search, an ID or URL.
        """
        await ctx.trigger_typing()

        vc = ctx.voice_client

        if not vc:
            await ctx.invoke(self.connect_)

        player = self.get_player(ctx)

        # If download is False, source will be a dict which will be used later to regather the stream.
        # If download is True, source will be a discord.FFmpegPCMAudio with a VolumeTransformer.
        await ctx.send(f"**Sto cercando** \U0001F50E >> `{search}`", delete_after=20)
        source = await YTDLSource.create_source(ctx, search, loop=self.bot.loop, stream=True)

        await player.queue.put(source)

    @commands.command(name='pause')
    async def pause_(self, ctx):
        """Pause the currently playing song."""
        vc = ctx.voice_client

        if not vc or not vc.is_playing():
            return await ctx.send('Non sto riproducendo nulla!', delete_after=20)
        elif vc.is_paused():
            return

        vc.pause()
        await ctx.send(f'**`{ctx.author}`** >> \U000023F8 __Ha **messo in pausa** il brano!__')

    @commands.command(name='resume')
    async def resume_(self, ctx):
        """Resume the currently paused song."""
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            return await ctx.send('Non sto riproducendo nulla!', delete_after=20)
        elif not vc.is_paused():
            return

        vc.resume()
        await ctx.send(f'**`{ctx.author}`** >> \U000025B6 __**Musica ripresa**!__')

    @commands.command(name='skip')
    async def skip_(self, ctx):
        """Skip the song."""
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            return await ctx.send('Non sto riproducendo nulla!', delete_after=20)

        if vc.is_paused():
            pass
        elif not vc.is_playing():
            return

        vc.stop()
        await ctx.send(f'**`{ctx.author}`** >> \U000023ED __Ha **cambiato brano**!__', delete_after=20)

    @commands.command(name='queue', aliases=['q', 'playlist'])
    async def queue_info(self, ctx):
        """Retrieve a basic queue of upcoming songs."""
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            return await ctx.send('Non sto riproducendo nulla!', delete_after=20)

        player = self.get_player(ctx)
        if player.queue.empty():
            return await ctx.send('\U0000274C **Non ci sono musiche in coda.**')

        # Grab up to 5 entries from the queue...
        upcoming = list(itertools.islice(player.queue._queue, 0, 5))

        fmt = '\n'.join(f'{i+1}. **`{_["title"]}`**' for i, _ in enumerate(upcoming))
        embed = discord.Embed(title=f'Coda, le prossime 5: {len(upcoming)}', description=fmt)

        await ctx.send(embed=embed)

    @commands.command(name='np', aliases=['now_playing', 'current', 'currentsong', 'playing'])
    async def now_playing_(self, ctx):
        """Display information about the currently playing song."""
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            return await ctx.send('Non sono collegato in alcun canale vocale!', delete_after=20)

        player = self.get_player(ctx)
        if not player.current:
            return await ctx.send('Non sto riproducendo nulla!')

        try:
            # Remove our previous now_playing message.
            await player.np.delete()
        except discord.HTTPException:
            pass

        player.np = await ctx.send(f'**`{vc.source.requester}`** >> '
                                   f'**Sto riproducendo** \U0001F3B6: `{vc.source.title}`', delete_after=20)

    @commands.command(name='volume', aliases=['vol'])
    async def change_volume(self, ctx, *, vol: float):
        """Change the player volume.

        Parameters
        ------------
        ctx: Discord's context
        vol: float or int [Required]
            The volume to set the player to in percentage. This must be between 1 and 100.
        """
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            return await ctx.send('Non sono collegato ad alcun canale vocale!', delete_after=20)

        if not 0 < vol < 101:
            return await ctx.send('Inserisci un numero compreso tra 0 e 100.')

        player = self.get_player(ctx)

        if vc.source:
            vc.source.volume = vol / 100

        player.volume = vol / 100
        await ctx.send(f'**`{ctx.author}`** >> \U0001F50A __Ha impostato il volume al **{vol}%**__', delete_after=20)

    @commands.command(name='stop')
    async def stop_(self, ctx):
        """Stop the currently playing song and destroy the player.

        !Warning!
            This will destroy the player assigned to your guild, also deleting any queued songs and settings.
        """
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            return await ctx.send('Non sto riproducendo nulla!', delete_after=20)

        await self.cleanup(ctx.guild)
        await ctx.send("\U0001F4EA **Disconnesso con successo!**", delete_after=20)


# Telegram On_Quiz_Event Callback function
@telegram_client.event("new_quiz")
async def on_quiz():
    bot.loop.create_task(bot.get_channel(settings.record_bot_join_channel).send("/quiz_alert"))

    # RANDOM USER QUIZ ALERT WEBHOOK (GREEN CHANNEL)

    # INVOCA IL COMANDO QUIZ STATS (genera un ctx fittizio)
    message = await bot.get_channel(settings.record_bot_join_channel).send("🎙️ **QUIZ!**")
    ctx = await bot.get_context(message)
    bot.loop.create_task(bot.get_command('quiz_stats').__call__(ctx))


@bot.command(hidden=True, description="Work in progress...")
@commands.guild_only()
@commands.has_permissions(administrator=True)
async def quiz_stats(ctx):

    # Lista stati globali:
    # - 0: Non presente nel canale vocale
    # Lista stati locali:
    # - 1: Presente
    # - 2: Presente mutato
    # - 3: Presente senza audio
    # - 4: Presente mutato e senza audio
    # Lista stati server:
    # - 5: Mutato da server
    # - 6: Senza audio da server
    # - 7: Mutato e senza audio da server

    # Lily31, valeve
    untouchable = [ctx.guild.get_member(user_id) for user_id in [, ]]
    untouchable_roles = [ctx.guild.get_role(role_id) for role_id in [, ,
                                                                     , ]]

    vet_role = ctx.guild.get_role()  # Ruolo Veterano
    green_role = ctx.guild.get_role()  # Ruolo Verificato

    quiz_time = datetime.datetime.now(tz) + datetime.timedelta(seconds=60)

    filtered_user = [{"user": user, "state": [], "event": [], "channel": None}
                     for user in ctx.guild.members if not user.bot]

    filtered_chanel = [settings.violet_main_channel, settings.green_main_channel]

    def find_dict(list_dict, member):
        try:
            index = [i for i, _ in enumerate(list_dict) if _["user"] == member][0]
            return index
        except IndexError:
            return

    @bot.event
    async def on_voice_state_update(member, before, after):
        if quiz_flag:
            index = find_dict(filtered_user, member)
            update_state(filtered_user[index], before)
        return after

    def update_state(diz, before):
        if before is not None:
            if before.channel is None:
                diz["state"].append(0)
            elif before.channel.id in filtered_chanel:
                if before.self_mute or before.mute:
                    diz["state"].append(2)
                else:
                    diz["state"].append(1)
                diz["channel"] = before.channel.id
            else:
                diz["state"].append(0)
        else:
            diz["state"].append(0)
        diz["event"].append(datetime.datetime.now())

    time_list = []
    start = datetime.datetime.now()

    quiz_flag = True
    while (datetime.datetime.now() - start).seconds < 770:  # Intervallo di listening
        time_list.append(datetime.datetime.now())
        await asyncio.sleep(1)
    quiz_flag = False

    veteran_list = []
    green_list = []

    for _diz in filtered_user:
        update_state(_diz, _diz["user"].voice)
        if _diz["user"] not in untouchable and _diz["user"].top_role not in untouchable_roles:
            if _diz["channel"] == filtered_chanel[1] or _diz["user"].top_role == green_role:
                green_list.append(_diz)
                if _diz["user"].top_role == vet_role:
                    state_len = [0] * len(_diz["event"])  # !! Channel??
                    veteran_list.append({"user": _diz["user"], "state": state_len, "event": _diz["event"]})
            elif _diz["channel"] == filtered_chanel[0] or _diz["user"].top_role == vet_role:
                veteran_list.append(_diz)

    time_list.append(datetime.datetime.now())

    gfilename, gpath = await image_plot.quiz_plot(green_role, quiz_time, time_list, green_list)
    bfilename, bpath = await image_plot.quiz_plot(vet_role, quiz_time, time_list, veteran_list)

    b_file = discord.File(bpath, filename=bfilename)
    g_file = discord.File(gpath, filename=gfilename)
    b_embed = discord.Embed(title=f'**{vet_role.name}   -   Panoramica del quiz**', colour=discord.Colour(0x61f1))
    g_embed = discord.Embed(title=f'**{green_role.name}   -   Panoramica del quiz**', colour=discord.Colour(0x7ed321))
    b_embed.add_field(name="Quando", value=f'```{quiz_time.strftime("%d/%m/%Y - %H:%M")}```')
    b_embed.add_field(name="Tag", value=f'```Quiz_{quiz_time.strftime("%d/%m/%Y_%H:%M")}```')
    g_embed.add_field(name="Quando", value=f'```{quiz_time.strftime("%d/%m/%Y - %H:%M")}```')
    g_embed.add_field(name="Tag", value=f'```Quiz_{quiz_time.strftime("%d/%m/%Y_%H:%M")}```')
    b_embed.set_image(url=f"attachment://{bfilename}")
    g_embed.set_image(url=f"attachment://{gfilename}")

    green_mod_channel = ctx.guild.get_channel()  # Canale moderatore verdi
    blu_mod_channel = ctx.guild.get_channel()  # Canale moderatori blu

    await asyncio.gather(
        blu_mod_channel.send(file=b_file, embed=b_embed),
        green_mod_channel.send(file=g_file, embed=g_embed)
    )

if __name__ == '__main__':
    tz = pytz.timezone('Europe/Rome')
    db_client = MongoDB(database="")
    image_plot = PlotUpload(client_id='',
                            client_secret='')

    telegram_client.run()
    bot.run('')
