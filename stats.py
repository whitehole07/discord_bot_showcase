import discord
from discord import Webhook, RequestsWebhookAdapter
import altair as alt
from altair_saver import save
import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.lines import Line2D
import matplotlib.font_manager as fm
import numpy as np
import requests
from base64 import b64encode
import asyncio


def stringify(univ_list):
    string = ''
    for elem in univ_list:
        string = string + elem + '\n'

    return string


def wrong_text_log(bot, message):
    import settings
    member = message.author
    user = member.name + "#" + member.discriminator
    log_channel = message.guild.get_channel(settings.rules_log_id)

    embed = discord.Embed(title="**Non ha accettato il regolamento** ❌", colour=discord.Colour(0xad04c1),
                          description=f"Ha scritto:```{message.content}```",
                          timestamp=message.created_at)

    embed.set_thumbnail(url=member.avatar_url)
    embed.set_author(name=user, icon_url=member.avatar_url)
    embed.set_footer(text=f"ID: {member.id}", icon_url=message.guild.icon_url)
    embed.add_field(name="Quando:", value=f"```{message.created_at.strftime('%d-%m-%Y %H:%M:%S')}```")
    embed.add_field(name="Nel canale:", value=f"```{message.channel.name}```")
    bot.loop.create_task(log_channel.send(embed=embed))


class PlotUpload:

    def __init__(self, client_id=None, client_secret=None):
        self.client_id = client_id
        self.client_secret = client_secret
        self.prop = fm.FontProperties(fname='fonts/NotoSerifJP-Regular.otf')

    def upload_image(self, source):
        client_id = self.client_id
        api_key = self.client_secret

        headers = {"Authorization": f"Client-ID {client_id}"}
        url = "https://api.imgur.com/3/upload.json"

        with open(source, 'rb') as f:
            image_file = b64encode(f.read())

        j1 = requests.post(
            url,
            headers=headers,
            data={
                'key': api_key,
                'image': image_file,
                'type': 'base64'
            }
        )
        image = j1.json()["data"]["link"]
        return image

    def tstats_embed_previw(self, plot_data,  upload=False, member_id=None):
        if not upload:
            assert member_id is not None

        filename = '{}_embed_preview.png'.format(member_id)
        my_path = os.path.abspath("images/text_stats/{}".format(member_id))
        path = os.path.join(my_path, filename)

        try:
            os.mkdir(my_path)
        except FileExistsError:
            pass

        alt_plot = {
            "x": [x for x in range(len(plot_data["y"]))],
            "y": plot_data["y"]
        }

        source = pd.DataFrame(alt_plot)

        chart = alt.Chart(source).mark_bar(
            cornerRadiusTopLeft=10,
            cornerRadiusTopRight=10,
            size=45,
            opacity=0.6,
            color=alt.Gradient(
                gradient='linear',
                stops=[alt.GradientStop(color='transparent', offset=0),
                       alt.GradientStop(color='yellow', offset=1)],
                x1=1,
                x2=1,
                y1=1,
                y2=0
            )
        ).encode(
            alt.X('x', axis=None),
            alt.Y('y', axis=None)
        ).properties(background='transparent', width=800, height=468).configure_view(
            strokeWidth=0
        )

        save(chart, path)
        if upload:
            return self.upload_image(path)
        else:
            return path

    def tstats_embed_detailed(self, plot_data, user: str, upload=False, member_id=None):
        if not upload:
            assert member_id is not None

        filename = '{}_embed_detailed.png'.format(member_id)
        my_path = os.path.abspath("images/text_stats/{}".format(member_id))
        path = os.path.join(my_path, filename)

        try:
            os.mkdir(my_path)
        except FileExistsError:
            pass

        params = {"ytick.color": "black",
                  "xtick.color": "black",
                  "axes.labelcolor": "black",
                  "axes.edgecolor": "black"}

        plt.rcParams.update(params)

        mute_color = 'orange'
        normal_color = 'green'
        fig, ax1 = plt.subplots()
        fig.set_size_inches(12.8, 7.2)

        # Hide the right and top spines
        ax1.spines['right'].set_visible(False)
        ax1.spines['top'].set_visible(False)

        bar = ax1.bar(plot_data["x"],
                      plot_data["y"],
                      width=0.45,
                      color=normal_color,
                      label='Messaggi regolari')

        ax1.set_xticks(plot_data["x"])
        ax1.set_xticklabels(plot_data["x"])
        ax1.set_xlabel('Mesi')
        ax1.set_ylabel('Numero di messaggi')
        ax1.yaxis.grid(color='gray', linestyle='dashed', alpha=0.2)
        ax1.xaxis.grid(color='gray', linestyle='dashed', alpha=0.2)
        ax1.set_title(f'Text Stats di: {user}', fontproperties=self.prop, fontsize=15)
        ax1.set_axisbelow(True)

        def autolabel(rects, x_offset: int, color):
            """Attach a text label above each bar in *rects*, displaying its height."""
            for rect in rects:
                height = rect.get_height()
                ax1.annotate('{}'.format(height),
                             xy=(rect.get_x() + rect.get_width() / 2, height),
                             xytext=(x_offset, 3),
                             textcoords="offset points",
                             ha='center', va='bottom',
                             color=color)

        autolabel(bar, -15, normal_color)
        fig.tight_layout()

        bar2 = ax1.bar(plot_data["x"],
                       plot_data["m"],
                       width=0.45,
                       color=mute_color,
                       label="In cui dice che si muterà")

        autolabel(bar2, 15, color=mute_color)

        plt.axvline(x=plot_data["j_index"],
                    ymin=0,
                    ymax=max(plot_data["y"]),
                    linestyle='dashed',
                    color='red',
                    alpha=0.5)

        ax1.annotate('Last Join',
                     xy=(plot_data["j_index"], max(plot_data["y"])),
                     xytext=(25, 3),
                     textcoords="offset points",
                     ha='center', va='bottom',
                     color='red')

        ax1.legend()

        plt.savefig(path, format='png', dpi=300)
        plt.close('all')
        if upload:
            return self.upload_image(path)
        else:
            return path

    async def quiz_plot(self, role, quiz_time, time_list, user_list):
        name = (''.join(c for c in role.name if c not in ' |')).encode('ascii', 'ignore').decode('ascii')
        filename = 'last_quiz_{}.png'.format(name)
        my_path = os.path.abspath("images/quiz_plot")
        path = os.path.join(my_path, filename)

        params = {"ytick.color": "w",
                  "xtick.color": "w",
                  "axes.labelcolor": "w",
                  "axes.edgecolor": "w"}

        plt.rcParams.update(params)

        bg_color = '#262736'
        users = [elem["user"].name.encode('ascii', 'ignore').decode('ascii') for elem in user_list]

        space_bet_bar = 2

        fig, ax = plt.subplots()
        fig.autofmt_xdate()
        fig.set_size_inches(12.8, 7.2)

        ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))
        xfmt = mdates.DateFormatter('%H:%M')
        ax.xaxis.set_major_formatter(xfmt)

        user_ticks = np.arange(0, len(user_list) * space_bet_bar, space_bet_bar)
        ax.set_yticks(user_ticks)
        ax.set_yticklabels(users, rotation=20, fontproperties=self.prop)
        ax.set_xlim(time_list[0], time_list[-1])
        #              rosso      verde    arancione
        lo_colors = ['#b00000', '#4cb000', '#de8100']

        parameters = {
            "align": 'center',
            "height": 1
        }

        for user in user_list:
            for state, event in zip(user["state"][::-1], user["event"][::-1]):
                ax.barh(user_list.index(user) * space_bet_bar, event,
                        align=parameters["align"],
                        color=lo_colors[state],
                        height=parameters["height"])

                await asyncio.sleep(.02)

        custom_lines = [Line2D([0], [0], color='#b00000', lw=4),
                        Line2D([0], [0], color='#de8100', lw=4),
                        Line2D([0], [0], color='#4cb000', lw=4),
                        ]

        leg = ax.legend(custom_lines, ['Assente', 'Mutato [Guild/Locale]', 'Presente'],
                        loc='upper center',
                        mode='expand',
                        ncol=4,
                        bbox_to_anchor=(0, 0.835, 1, 0.2),
                        fancybox=True,
                        framealpha=1.0,
                        facecolor=bg_color,
                        prop=self.prop)

        for text in leg.get_texts():
            text.set_color('white')

        ax.set_facecolor(bg_color)
        ax.xaxis.grid(color='white', linestyle='dashed', alpha=0.4)
        ax.set_axisbelow(False)
        ax.set_xlabel('Tempo', fontproperties=self.prop)
        ax.set_title(f'Panoramica sul Quiz del {quiz_time.strftime("%d/%m/%Y - %H:%M")}  [{name.upper()}]',
                     pad=22, color='white', fontproperties=self.prop, fontsize=15)

        for label in (ax.get_xticklabels()):
            label.set_fontproperties(self.prop)

        fig.tight_layout()
        plt.savefig(path, facecolor=bg_color, format='png', dpi=300)
        plt.close('all')

        return filename, path


class RandomUserWebHook:

    def __init__(self, bot, db_client, wh_id: int, wh_token: str):
        self.bot = bot
        self.guild = self.bot.get_guild()
        self.db_client = db_client
        self.wh = Webhook.partial(wh_id, wh_token, adapter=RequestsWebhookAdapter())

    def post(self):
        randoc = self.db_client.random_doc(str(self.guild.id) + '.alert_dict')
        member = self.guild.get_member(randoc["user_id"])
        self.wh.send(randoc["phrase"], username=f'{member.name} Bot', avatar_url=member.avatar_url)
